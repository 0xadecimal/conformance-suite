
### v1.1.9 (7th June 2019)

The release is called **v1.1.9**, it adds support kid/trust anchor using EIDAS certificates and enables the Docker Content Trust Framework.

[Full Release Notes](docs/releases/v1.1.9.md) (v1.1.9.md)

### v1.1.8 (29th May 2019)

Release **v1.1.8**, fixes Payment template bugs, adds payments tests, reduces log noise

[Full Release Notes](docs/releases/v1.1.8.md) (v1.1.8.md)

### v1.1.7 (16th May 2019)

Release **v1.1.7**, fixes UI bugs and makes currency and payment amount configurable in the UI.

[Full Release Notes](docs/releases/v1.1.7.md) (v1.1.7.md)

### v1.1.6 (9th May 2019)

Release **v1.1.6** adds `private_key_jwt` as a client authentication method, addresses issues with Azure and TLS renegotiation and increases test coverage by fixing a known issue with the swagger validation.

[Full Release Notes](docs/releases/v1.1.6.md) (v1.1.6.md)

### v1.1.5 (26th April 2019)

Release v1.1.5 addresses known issues with the signing of payments requests, payment headers and also, adds several reporting enhancements.

[Full Release Notes](docs/releases/v1.1.5.md) (v1.1.5.md)

### v1.1.4 (17th April 2019)

Release v1.1.4 addresses known issues with PIS payment message signing.

[Full Release Notes](docs/releases/v1.1.4.md) (v1.1.4.md)

### v1.1.1 (8th April 2019)

Release v1.1.1 addresses permission mapping issues and other bugs following feedback from users at the OBIE Functional Workshop.

[Full Release Notes](docs/releases/v1.1.1.md) (v1.1.1.md)

### v1.1.0 (4th April 2019)

Release v1.1.0 addresses known issues following feedback from users at the OBIE Functional Workshop.

[Full Release Notes](docs/releases/v1.1.0.md) (v1.1.0.md)

### v1.0.0-rc1 (29th March 2019)

This **v1.0.0-rc1** release introduces manual PSU consent (hybrid flow) for for v3.1 of the OBIE Accounts and Transactions specifications and Payments.

[Full Release Notes](docs/releases/v1.0.0.md) (v1.0.0-rc1.md)

---

[More Releases](docs/releases)